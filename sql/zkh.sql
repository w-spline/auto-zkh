--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.3

-- Started on 2019-07-27 20:57:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2984 (class 1262 OID 16482)
-- Name: zkh; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE zkh WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru-RU@icu' LC_CTYPE = 'ru-RU';


SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 3079 OID 16483)
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- TOC entry 2985 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- TOC entry 2 (class 3079 OID 16560)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 2986 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- TOC entry 259 (class 1255 OID 16651)
-- Name: after_new_issue(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.after_new_issue() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	INSERT INTO issues_status_history(id,date,status,active, issue)
	VALUES (uuid_generate_v4(), NEW.date, '76e675db-b50f-43a9-bad9-abd3a2c2a331', true, NEW.id);
END;$$;


--
-- TOC entry 258 (class 1255 OID 16637)
-- Name: new_issue(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.new_issue() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	NEW.ip = uuid_generate_v4();
	
	RETURN NEW;
END;$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 200 (class 1259 OID 16579)
-- Name: category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.category (
    id uuid NOT NULL,
    value character varying
);


--
-- TOC entry 203 (class 1259 OID 16609)
-- Name: client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.client (
    id uuid NOT NULL,
    fam character varying,
    im character varying,
    ot character varying,
    email character varying,
    address uuid,
    phone character varying
);


--
-- TOC entry 202 (class 1259 OID 16601)
-- Name: issue_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issue_status (
    id uuid NOT NULL,
    value character varying,
    active_period interval
);


--
-- TOC entry 204 (class 1259 OID 16617)
-- Name: issues; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues (
    id uuid NOT NULL,
    created_at timestamp with time zone,
    client uuid,
    category uuid,
    description character varying
);


--
-- TOC entry 205 (class 1259 OID 16638)
-- Name: issues_status_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues_status_history (
    id uuid NOT NULL,
    date timestamp with time zone,
    status uuid,
    active boolean,
    issue uuid
);


--
-- TOC entry 199 (class 1259 OID 16571)
-- Name: operators; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.operators (
    id uuid NOT NULL,
    name character varying,
    dis_name character varying
);


--
-- TOC entry 201 (class 1259 OID 16587)
-- Name: sub_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_category (
    id uuid NOT NULL,
    category uuid,
    value character varying
);


--
-- TOC entry 2973 (class 0 OID 16579)
-- Dependencies: 200
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.category VALUES ('7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Квартира');
INSERT INTO public.category VALUES ('9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Подьезд');
INSERT INTO public.category VALUES ('73e6d3f0-f269-4f75-9e07-f58862906da7', 'Дом');
INSERT INTO public.category VALUES ('0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Двор');


--
-- TOC entry 2976 (class 0 OID 16609)
-- Dependencies: 203
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2975 (class 0 OID 16601)
-- Dependencies: 202
-- Data for Name: issue_status; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.issue_status VALUES ('bef9fc1a-6973-4cff-8997-f8af6e0d06d7', 'Отклонена', NULL);
INSERT INTO public.issue_status VALUES ('6b12fbd5-571a-492b-a03f-e116c1439ee5', 'Выполнена', NULL);
INSERT INTO public.issue_status VALUES ('76e675db-b50f-43a9-bad9-abd3a2c2a331', 'Новая', '3 days');
INSERT INTO public.issue_status VALUES ('8c36e53c-27b0-4625-9618-dd78da171a72', 'Зарегистрирована', '30 days');
INSERT INTO public.issue_status VALUES ('18c7fe6e-0dae-49f4-8d2c-37aca31bd72c', 'Выполнение продлено', '60 days');


--
-- TOC entry 2977 (class 0 OID 16617)
-- Dependencies: 204
-- Data for Name: issues; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2978 (class 0 OID 16638)
-- Dependencies: 205
-- Data for Name: issues_status_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2972 (class 0 OID 16571)
-- Dependencies: 199
-- Data for Name: operators; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2974 (class 0 OID 16587)
-- Dependencies: 201
-- Data for Name: sub_category; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sub_category VALUES ('4184dd60-168f-4318-8ddf-d8a7360f41ce', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Плесень на стенах');
INSERT INTO public.sub_category VALUES ('d6f13654-cf03-4c91-9a15-b8df8cd09250', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Низкая температура горячей воды');
INSERT INTO public.sub_category VALUES ('8db13a03-7df0-488d-a050-190ecae9515c', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Некачественная вода');
INSERT INTO public.sub_category VALUES ('db43670f-6696-4ec8-870c-66597f3a6437', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Проблемы с канализацией');
INSERT INTO public.sub_category VALUES ('4b8801cd-65a8-4f0a-9f5e-c95d06ced651', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Холодный полотенцесушитель');
INSERT INTO public.sub_category VALUES ('45c121eb-cb2e-4cbc-884c-5329557b3c31', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Слабый напор воды');
INSERT INTO public.sub_category VALUES ('f433d29c-1986-416b-aac1-bb4d4fffddec', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Перепады напряжения');
INSERT INTO public.sub_category VALUES ('aaa0b639-1e26-478c-8e99-7bc4e1a72200', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Не работает вентиляция');
INSERT INTO public.sub_category VALUES ('cdb1a0fd-2402-495d-adec-249feacc4e42', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Не топят батареи');
INSERT INTO public.sub_category VALUES ('21e6f3d4-768e-435c-8d9f-2a7775b2d306', '7a80e5c3-1ab5-43b5-99ab-a417c4a18638', 'Другая');
INSERT INTO public.sub_category VALUES ('dc091a2a-6601-4cc7-9e70-3c3b78d386c2', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Надписи на стенах');
INSERT INTO public.sub_category VALUES ('f5a65a60-be52-47c2-a622-2a88158721e8', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Разбита лампочка');
INSERT INTO public.sub_category VALUES ('7c41a43c-ac61-4d4b-995c-bd2cca8fbb1b', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Грызуны и насекомые');
INSERT INTO public.sub_category VALUES ('4ffaa451-682a-430b-9772-5f4e8633109c', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Расклеены объявления');
INSERT INTO public.sub_category VALUES ('90350938-ae3c-4c30-bae4-a014551b5697', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Треснула плитка на полу');
INSERT INTO public.sub_category VALUES ('d522b4a2-a34c-4e70-b946-f7b75cf5ad36', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Повреждены ступени');
INSERT INTO public.sub_category VALUES ('7505c79f-9c0c-49e5-ac76-934874397dae', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Засор мусоропровода');
INSERT INTO public.sub_category VALUES ('09bd85f7-d2e8-44a8-b0fe-8f108f592fc0', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Грязный лифт');
INSERT INTO public.sub_category VALUES ('220ebbfd-f9dc-4307-86ab-4c4f46d3c5d4', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Сломан почтовый ящик');
INSERT INTO public.sub_category VALUES ('d856c9ca-e9bc-409d-a44b-9172cfe5e043', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Погнуты перила лестницы');
INSERT INTO public.sub_category VALUES ('e351f1b8-3b38-4359-9fb8-82c7ab126ab7', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Не вымыты полы');
INSERT INTO public.sub_category VALUES ('ca7d76fe-9c3f-4e59-9662-d4fe863873f2', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Разбито окно');
INSERT INTO public.sub_category VALUES ('81238118-7684-4e64-bf9a-1a449a53ad21', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Запах из подвала');
INSERT INTO public.sub_category VALUES ('1733d24c-0673-4003-8c33-05ba54abb4b5', '9c0ff08e-27e0-45e5-ae6e-a40c200915af', 'Другая');
INSERT INTO public.sub_category VALUES ('9ef2473c-346a-4e81-8313-4819c7421dba', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Осыпается балкон');
INSERT INTO public.sub_category VALUES ('bab66c96-6187-4c09-ba97-9691ac70f862', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Надписи на фасаде');
INSERT INTO public.sub_category VALUES ('d842dffd-2a44-4faf-be3f-5dea7ba23525', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Протекает крыша');
INSERT INTO public.sub_category VALUES ('f0f28444-5c00-46e3-af67-8a9c6e7f9daf', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Водосточная труба');
INSERT INTO public.sub_category VALUES ('538d64e6-b20b-417f-878a-3f952c117d5f', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Не убран снег');
INSERT INTO public.sub_category VALUES ('d2667ccc-bd4e-469d-a79a-2cd91ff6ff62', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Межпанельные швы');
INSERT INTO public.sub_category VALUES ('c4649604-59ed-4854-b74c-ac450da11fb0', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Гололед');
INSERT INTO public.sub_category VALUES ('37929daa-6280-4720-a341-97bf39c31b8e', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Образуются сосульки');
INSERT INTO public.sub_category VALUES ('290ed352-00ec-4869-b95f-29ac2f70d55f', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Разрушается козырек');
INSERT INTO public.sub_category VALUES ('a53a44e5-ceb5-457e-912a-6b0036c65227', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Трещины в фундаменте');
INSERT INTO public.sub_category VALUES ('7f8381c3-07f5-42a3-bec6-afc87cfa413b', '73e6d3f0-f269-4f75-9e07-f58862906da7', 'Другая');
INSERT INTO public.sub_category VALUES ('d86e3065-0cfb-4cde-8fb8-871de21735f2', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Недостаточное освещение');
INSERT INTO public.sub_category VALUES ('cee8d739-bf55-4c26-b987-0b4c2bad7b98', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Сломана детская площадка');
INSERT INTO public.sub_category VALUES ('e4a5de35-958f-4b97-9054-0fd648f01a78', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Несвоевременный вывоз мусора');
INSERT INTO public.sub_category VALUES ('6edf2f85-d9bf-4328-a1e7-80312a92d0b1', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Ямы и ухабы');
INSERT INTO public.sub_category VALUES ('2e4b7ea6-ab31-4eda-8478-ae3e0ed1bc8e', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Плохая уборка территории');
INSERT INTO public.sub_category VALUES ('79e714e8-1b4b-453b-82fe-559c113688bb', '0f2137a4-e864-4faf-b2ff-2d20d08ef4a3', 'Другая');


--
-- TOC entry 2827 (class 2606 OID 16586)
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2834 (class 2606 OID 16616)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 2832 (class 2606 OID 16608)
-- Name: issue_status issue_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issue_status
    ADD CONSTRAINT issue_status_pkey PRIMARY KEY (id);


--
-- TOC entry 2838 (class 2606 OID 16624)
-- Name: issues issues_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- TOC entry 2842 (class 2606 OID 16642)
-- Name: issues_status_history issues_status_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_status_history
    ADD CONSTRAINT issues_status_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2825 (class 2606 OID 16578)
-- Name: operators operators_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.operators
    ADD CONSTRAINT operators_pkey PRIMARY KEY (id);


--
-- TOC entry 2830 (class 2606 OID 16594)
-- Name: sub_category sub_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_category
    ADD CONSTRAINT sub_category_pkey PRIMARY KEY (id);


--
-- TOC entry 2835 (class 1259 OID 16636)
-- Name: fki_fk_issues_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_issues_category ON public.issues USING btree (category);


--
-- TOC entry 2836 (class 1259 OID 16630)
-- Name: fki_fk_issues_client; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_issues_client ON public.issues USING btree (client);


--
-- TOC entry 2839 (class 1259 OID 16659)
-- Name: fki_fk_issues_status_history_issue; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_issues_status_history_issue ON public.issues_status_history USING btree (issue);


--
-- TOC entry 2840 (class 1259 OID 16648)
-- Name: fki_fk_issues_status_history_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_issues_status_history_status ON public.issues_status_history USING btree (status);


--
-- TOC entry 2828 (class 1259 OID 16600)
-- Name: fki_fki_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fki_category ON public.sub_category USING btree (category);


--
-- TOC entry 2848 (class 2620 OID 16653)
-- Name: issues trg_after_new_issue; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_after_new_issue AFTER INSERT ON public.issues FOR EACH ROW EXECUTE PROCEDURE public.after_new_issue();


--
-- TOC entry 2849 (class 2620 OID 16652)
-- Name: issues trg_new_issue; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trg_new_issue BEFORE INSERT ON public.issues FOR EACH ROW EXECUTE PROCEDURE public.new_issue();


--
-- TOC entry 2845 (class 2606 OID 16631)
-- Name: issues fk_issues_category; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_issues_category FOREIGN KEY (category) REFERENCES public.category(id);


--
-- TOC entry 2844 (class 2606 OID 16625)
-- Name: issues fk_issues_client; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_issues_client FOREIGN KEY (client) REFERENCES public.client(id);


--
-- TOC entry 2847 (class 2606 OID 16654)
-- Name: issues_status_history fk_issues_status_history_issue; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_status_history
    ADD CONSTRAINT fk_issues_status_history_issue FOREIGN KEY (issue) REFERENCES public.issues(id);


--
-- TOC entry 2846 (class 2606 OID 16643)
-- Name: issues_status_history fk_issues_status_history_status; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_status_history
    ADD CONSTRAINT fk_issues_status_history_status FOREIGN KEY (status) REFERENCES public.issue_status(id);


--
-- TOC entry 2843 (class 2606 OID 16595)
-- Name: sub_category fki_category; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_category
    ADD CONSTRAINT fki_category FOREIGN KEY (category) REFERENCES public.category(id);


-- Completed on 2019-07-27 20:57:46

--
-- PostgreSQL database dump complete
--

