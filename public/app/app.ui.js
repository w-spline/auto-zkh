
var menu_data = [
    {id: "issues", icon: "fas fa-exclamation ", value: "Заявки"},
    {id:"issue_detail", icon : "fa fa-id-card",  value: 'Текущая заявка',disabled:true},
    {id: "issues_statistic", icon : "fa fa-chart-bar", value: "Статистика заявок"},

]

var mainWND = {
    rows: [
        {
            view: "toolbar", css: "webix_dark", padding: 3, elements: [
                {
                    view: "button", type: "icon", icon: "fa fa-bars" /*"mdi mdi-menu"*/,
                    width: 37, align: "left", css: "app_button", click: function () {
                        $$("sidebar").toggle();
                    }
                },
                {view: "label", label: "ЖКХ. Панель оператора"},
                {},
            ]
        },
        { cols:[
                {
                    view: "sidebar",
                    id: 'sidebar',
                    css:"webix_dark",
                    collapsed:true,
                    data: menu_data,
                    on:{
                        onAfterSelect: function(id){
                            app.menu_selected(id)
                        }
                    }
                },
                {
                    view : 'multiview',
                    id : 'view',
                    cells : [
                        {
                            id : 'welcome',
                            template: ''
                        }
                    ]
                }
            ]}
    ]
}

