
var app = {

    _current_issue : '',

    init : function () {


        webix.ui(
            mainWND
        )




        webix.storage.local.clear();
        webix.i18n.setLocale('ru-RU');
        webix.Date.startOnMonday = true;
    },

    menu_selected : function (val) {
        switch (val) {
            case "issues" :
                app.issues()
                break
            case "issues_statistic" :
                app.issues_statistic()
                break
            case "issue_detail" :
                app.issue_detail()
                break
        }
    },

    issues : function () {

        var wnd = this
        if (!$$('issues')){
            $$('view').addView({
                id : 'issues',
                view : 'issues',
                on: {
                    'issueSelected' : webix.bind(function (id) {
                        this._current_issue = id
                        console.log(id)

                        //this.issue_detail()
                        $$('sidebar').select('issue_detail')
                    },this)

                }
            })
        }
        $$('issues').show()

    },

    issues_statistic : function () {
        if(!$$('issues_statistic')){
            $$('view').addView({
                id : 'issues_statistic',
                view : 'issues_statistic',
            })
        }
        $$('issues_statistic').show()
    },

    issue_detail : function () {
        var wnd = this
        if(!$$('issue_detail')){
            $$('view').addView({
                id : 'issue_detail',
                view : 'issue_detail',
                //issue : wnd._current_issue
            })
        }
        $$('issue_detail').load(wnd._current_issue)
        $$('issue_detail').show()
    }

    
}