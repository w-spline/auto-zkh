webix.protoUI({
    name : 'issues',
    $init : function (config) {
        config.rows = [
            {
                view : 'toolbar',
                elements : [
                    {
                        view : "label",
                        label : "Заявки",

                    },
                    {},
                    {
                        view : 'button',
                        label : 'Обновить',
                        type : "icon",
                        icon : 'fa fa-sync',
                        autowidth : true,
                        click : webix.bind(function () {
                            this.update()
                        },this)
                    }
                ]
            },
            {
                view : 'datatable',
                css:"webix_data_border webix_header_border",
                url:"/issues/list",
                id: 'issues',
                select :  "row",
                columns : [
                    {id:"id",hidden:true},
                    {id:"status",header:"Статус заявки", width : 140, template: "#status_name#"},
                    {id:"created_at",header:'Дата заявки', width : 140, format:webix.i18n.fullDateFormatStr},
                    {id:"wait_date",header:'Ожидание решения', width : 140, format:webix.i18n.fullDateFormatStr},
                    {id:"sub_category",header:"Категория", template: "#sub_category_name#",width : 150},
                    {id:"client", header:'Клиент',template:"#fam# #im# #ot#", width : 200},
                    {id:"address",header:"Адрес",width: 240},
                    {id:'description',header:'Описание заявки',fillspace:true},
                    {id:'rank', header:'Приоритет'}
                ],
                on : {
                    onAfterLoad : webix.bind(function () {
                        if(this.$$('issues').count()){
                            this.$$('issues').select(
                                this.$$('issues').getFirstId()
                            )
                        }
                    },this),

                    onItemDblClick : webix.bind(function (item) {
                        this.callEvent("issueSelected",[item.row])
                        console.log(item)

                    },this)
                }
            }
        ]
    },

    update : function () {

    }

},webix.IdSpace,webix.ui.layout)