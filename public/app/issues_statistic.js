webix.protoUI({
    name : "issues_statistic",
    $init : function (config) {
        config.cols = [
            {
                view : "datatable",
                id : "categories",
                select : "row",
                url : '/issue/category_with_count',
                columns : [
                    {id:'id',hidden:true},
                    {id:'value',header:"Категория",fillspace:true},
                    {id:'issue_count',header:"Коли-во инцидентов"}
                ]
            },
            {
                view : "datatable",
                id : "sub_categories",
                select : "row",
                columns : [
                    {id:'id',hidden:true},
                    {id:'value',header:"Подкатегории",fillspace:true},
                    {id:'issue_count',header:"Коли-во инцидентов"}
                ]
            }
        ]
    }

},webix.IdSpace,webix.ui.layout)