webix.protoUI({
    name : "issue_detail",
    _current_issue : '',
    $init : function (config) {
        config.rows = [
            {
                view : 'form',
                id : 'form',
                elements : [
                    {
                        cols : [
                            {
                                view : 'button',
                                value : 'Зарегистировать заявку',
                                css : 'webix_primary',
                                autowidth : true,
                                click : webix.bind(function () {
                                    this.accept()
                                },this)
                            },
                            {
                                view : 'button',
                                value : 'Отклонить заявку',
                                css : 'webix_danger',
                                autowidth: true,
                                click : webix.bind(function () {
                                    this.cancle()
                                },this)
                            },
                            {

                            }
                        ]
                    },
                    {
                        view : 'template' ,
                        id : 'issue_info' ,
                        autoheight : true,
                        borderless : true
                    },
                    {

                    }
                ]
            }
        ]

        this.$ready.push(this._init);

    },

    _init : function(){
        
        var wnd = this;
        webix.extend(wnd.$$("form"),webix.ProgressBar)


    },

    load : function (id) {
        this._current_issue = id
        var wnd = this
        wnd.$$("form").showProgress({
            type:"icon"
        })
        webix.ajax().post("/issues/get",{id:id}).then(function (data) {
            wnd.$$("form").hideProgress();
            var data_p = data.json()

            var val = "<div  style='font-size: 14px'>"
            val+="<h1>Заявка</h1>  <br/>";

            val +="<b>Клиент </b>: "+data_p.fam + " " + data_p.im + " " + data_p.ot +"<br/>"

            val += "<b>Категория</b> : "+data_p.category_name + "/ " + data_p.sub_category_name + "<br/>"

            val += "<b>Описание проблемы: </b> " + data_p.description + "<br/>"

            val += "<b>Проблема обнаружена у : </b> "+ data_p.rank

            val += "</div>"


            wnd.$$('issue_info').define("template",val);
            wnd.$$('issue_info').refresh();

        })
    },

    accept : function(){
        var wnd = this;
        webix.ajax().post("/issues/accept",{id:wnd._current_issue}).then(function (data) {

        })

    },

    cancle : function(){

        webix.ajax().post("/issues/cancle",{id:wnd._current_issue}).then(function (data) {

        })

    },

    issue_setter : function (val) {

    }
    
},webix.IdSpace,webix.ui.layout)