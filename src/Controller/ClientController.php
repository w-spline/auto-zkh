<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClientController
 * @package App\Controller
 * @Route("/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/client", name="client")
     */
    public function index()
    {
        return $this->render('client/index.html.twig', [
            'controller_name' => 'ClientController',
        ]);
    }


    /**
     * @Route("/new_client")
     * @param Request $request
     * @param Connection $connection
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function new_client(Request $request,Connection $connection){
        $fam = $request->get('fam');
        $im = $request->get('im');
        $ot = $request->get('ot');
        $phone = $request->get('phone');
        $email = $request->get('email');
        $address = $request->get('address');

        $str = "with tm_client as (
                INSERT INTO public.client(
                    id, fam, im, ot, email,  phone, address_str)
                    VALUES (uuid_generate_v4(), ?, ?, ?, ?,  ?, ?)
                    RETURNING id
                    )
                select id from tm_client";
       // $p = $connection->executeQuery($str,[$fam,$im,$ot,$email,$phone]);
        $p = $connection->fetchAssoc($str,[$fam,$im,$ot,$email,$phone,$address]);

        return $this->json($p);

    }

    /**
     * @Route("/check_client")
     * @param Request $request
     * @param Connection $connection
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function check_client(Request $request,Connection $connection){
        $client = $request->get('client');
        $str = "SELECT *
            FROM public.client
            where id=?";
        $p = $connection->fetchAll($str,[$client]);
        return $this->json($p);
    }


    /**
     * @Route("/my_issues")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function my_issues(Request $request,Connection $connection){
        $client = $request->get('client');
        $str = "SELECT issues.id as issue_id, issues.created_at, issues.client, issues.sub_category, issues.description,
        issues_status_history.status, issue_status.value as status_name,
        sub_category.id as sub_category_id, sub_category.value as sub_category_name,
        category.id as category_id, category.value as category_name
	FROM public.issues
	left join issues_status_history on (issues_status_history.issue=issues.id and issues_status_history.active)
	left join issue_status on (issue_status.id=issues_status_history.status)
	left join public.sub_category ON sub_category.id = issues.sub_category
	left join public.category ON category.id = sub_category.category
	where issues.client=?";
        $p = $connection->fetchAll($str,[$client]);
        return $this->json($p);
    }

}
