<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IssuesController
 * @package App\Controller
 * @Route("/issues")
 */
class IssuesController extends AbstractController
{
    /**
     * @Route("/issues", name="issues")
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request,Connection $connection)
    {
        return '';
    }

    /**
     * @Route("/categories")
     * @param Connection $connection
     * @return JsonResponse
     */
    public function category(Connection $connection){
        $data = $connection->fetchAll("SELECT  * from category order by ord");

        return $this->json($data);
    }

    /**
     * @Route("/sub_categories")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function sub_category(Request $request,Connection $connection){
        $category = $request->get('category');
        $date = $connection->fetchAll("SELECT * from sub_category where category=?",[$category]);
        return $this->json($date);
    }

    /**
     * @Route("/all_sub_category")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function all_sub_category(Request $request,Connection $connection){

        $date = $connection->fetchAll("SELECT sub_category.id, sub_category.category, sub_category.value, category.value cat_value
                                    FROM sub_category
                                    left join category on (category.id=sub_category.category)");
        return $this->json($date);
    }


    /**
     * @Route("/new_issue")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function new_issue(Request $request,Connection $connection){
        $client = $request->get('client');
        $sub_category = $request->get('sub_category');
        $issue_descr = $request->get('issue_descr');
        $address = $request->get('address');

        $str = "with iss as  (
            INSERT INTO public.issues(
                id, created_at, client, sub_category, description, address_str)
                VALUES (uuid_generate_v4(), now(), ?, ?, ?, ?)
                RETURNING id
                )
            select id from iss";
        $p = $connection->fetchAssoc($str,[$client,$sub_category,$issue_descr,$address]);


        return $this->json($p);
    }

    /**
     * @Route("/issue_status")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function issue_status(Request $request,Connection $connection){
        $id = $request->get('id');
        $str = "SELECT issues_status_history.id, issues_status_history.date, issue_status.value as status_name, issues_status_history.active
                FROM public.issues_status_history
                left join issue_status on (issue_status.id=issues_status_history.status)
                order by issues_status_history.date";
        $data = $connection->fetchAll($str,[$id]);
        return $this->json($data);
    }

    /**
     * @Route("/upper_issue")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function upper_issue(Request $request, Connection $connection){
        $id = $request->get('id');
        $str = "UPDATE public.issues SET  rank=rank+1 	WHERE id=?";
        $p = $connection->executeUpdate($str,[$id]);
        return $this->json($p);
    }

    /**
     * @Route("/issues_by_address")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function issues_by_address(Request $request, Connection $connection){
        $address = $request->get('address');
        $str = "SELECT issues.id as issue_id, issues.created_at, issues.client, issues.sub_category, issues.description,
        issues_status_history.status, issue_status.value as status_name,
        sub_category.id as sub_category_id, sub_category.value as sub_category_name,
        category.id as category_id, category.value as category_name
	FROM public.issues
	left join issues_status_history on (issues_status_history.issue=issues.id and issues_status_history.active)
	left join issue_status on (issue_status.id=issues_status_history.status)
	left join public.sub_category ON sub_category.id = issues.sub_category
	left join public.category ON category.id = sub_category.category
	where upper(issues.address_str) like ? 
	and issues_status_history.status not in ('bef9fc1a-6973-4cff-8997-f8af6e0d06d7','6b12fbd5-571a-492b-a03f-e116c1439ee5')";

        $p = $connection->fetchAll($str,[mb_strtoupper($address."%")]);
        return $this->json($p);
    }

    /**
     * @Route("/list")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     */
    public function list(Request $request,Connection $connection){
        $str = "SELECT issues.id as id, issues.id as issue_id, issues.created_at, 
issues.created_at+'30 day' as wait_date,
        issues.client, issues.sub_category, issues.description,
        issues_status_history.status, issue_status.value as status_name,
        sub_category.id as sub_category_id, sub_category.value as sub_category_name,
        category.id as category_id, category.value as category_name,
        issues.address_str as address,
        client.fam, client.im, client.ot, issues.rank
	FROM public.issues
	left join issues_status_history on (issues_status_history.issue=issues.id and issues_status_history.active)
	left join issue_status on (issue_status.id=issues_status_history.status)
	left join public.sub_category ON sub_category.id = issues.sub_category
	left join public.category ON category.id = sub_category.category
	left join public.client ON client.id = issues.client ";

        $p = $connection->fetchAll($str,[]);

        return $this->json($p);

    }


    /**
     * @Route("/get")
     * @param Request $request
     * @param Connection $connection
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function get_info(Request $request,Connection $connection){
        $id = $request->get('id');
        $str = "SELECT issues.id as id, issues.id as issue_id, issues.created_at, issues.client, issues.sub_category, issues.description,
        issues_status_history.status, issue_status.value as status_name,
        sub_category.id as sub_category_id, sub_category.value as sub_category_name,
        category.id as category_id, category.value as category_name,
        issues.address_str as address,
        client.fam, client.im, client.ot, issues.rank
	FROM public.issues
	left join issues_status_history on (issues_status_history.issue=issues.id and issues_status_history.active)
	left join issue_status on (issue_status.id=issues_status_history.status)
	left join public.sub_category ON sub_category.id = issues.sub_category
	left join public.category ON category.id = sub_category.category
	left join public.client ON client.id = issues.client
	where issues.id=?";
        $p = $connection->fetchAssoc($str,[$id]);
        return $this->json($p);
    }


    public function accept(Request $request,Connection $connection){

         return $this->json(['res'=>1]);

    }

    public function cancle(Request $request,Connection $connection){
        return $this->json(['res'=>1]);

    }
}
